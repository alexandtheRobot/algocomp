# AlgoComp

### Algorithmic composition software 
***
### Setup

Requirements: You will need a copy of pure data extended install on your system

* Open AlgoComp.pd

* In the control section. Set your desired tempo, key and scale.
*Adjust the variation and range controls. NOTE: if these controls are off, no musical data will be produced.
***
* In the Drum, Bass and Melody section. Adjust the density and offset controls. 
* Drums are setup from left to right: kick, snare, hi hat, crash.
* Once you have set all the controls press the *REGEN* button in the control section to create your composition. NOTE: You can re-generate individual patterns using the small buttons located on each instrument strip.
* You've now created your first composition.
***
* In the mixer section. Make sure all channels are turned on and set all mix levels, pan and decay settings.
* To hear playback make sure the master volume is up on the control section and that DSP is enabled.
* Press I/O in the control section to begin playback.
* You should now be hearing sound.
***
* Use the pattern selector in the control section to preview your generated patterns.
* To animate your composition, stop playback.
* Enable the animator and select a pattern sequence.
* Enable the Tempo, Mix or Decay animators and dial in how much variation you'd like on each parameter.
* Regenerate patterns creates a new set of patterns automatically after 24 bars.
* Enable playback to see the animator is action.

***

* Alex Dudley
* alex.w.dudley@gmail.com